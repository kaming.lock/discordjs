import mongoose from "mongoose";
const Schema = mongoose.Schema;

const eventSchema = new Schema({
  msgID: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  limit: {
    type: Number,
    required: true,
    default: 15,
  },
  time: {
    type: String,
    required: true,
  },
  times: {
    type: String,
    required: false,
  },
  participants: {
    type: [String],
    required: false,
  },
});

export default mongoose.model("Event", eventSchema);
// module.exports = mongoose.model("Event", eventSchema);
