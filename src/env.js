import { config } from "dotenv";
import raids from "./assets/json/raids.json" assert { type: "json" };

config();

const TOKEN = process.env.DISCORDJS_BOT_TOKEN;
const CLIENT_ID = process.env.CLIENT_ID;
const GUILD_ID = process.env.GUILD_ID;
const CHANNEL_ID = process.env.CHANNEL_ID;
const pSrvCode = process.env.P_SRV_CODE;
const pSrvCode_SUFFIX = pSrvCode === "" ? "" : "_" + pSrvCode;
const isOfficial = pSrvCode === "";
const LANG = process.env.LNG ?? "en";
const LANG_SUFFIX = LANG === "en" ? "" : "_" + LANG;
const DATABASE_URI = process.env.DATABASE_URI;

const getRaidsInLang = (keyword = "") => {
  let data = raids;
  Object.keys(data).forEach((key, idx) => {
    data[key]["name"] = data[key]["name" + LANG_SUFFIX];
  });
  if (keyword == "") return data;
  for (let obj of data) {
    if (obj["value"] === keyword) {
      data = obj;
      break;
    }
  }
  return data;
};

export default {
  TOKEN,
  CLIENT_ID,
  GUILD_ID,
  CHANNEL_ID,
  pSrvCode,
  pSrvCode_SUFFIX,
  isOfficial,
  LANG,
  LANG_SUFFIX,
  DATABASE_URI,
  getRaidsInLang,
};
