import env from "./../env.js";
import { SlashCommandBuilder, SlashCommandStringOption } from "discord.js";

const newEvtCommand = new SlashCommandBuilder()
  .setName("newevent")
  .setDescription("起一個新活動")
  .addStringOption((option) =>
    option
      .setName("活動名稱")
      .setDescription("揀你要準備開始嘅團")
      .setRequired(true)
      .setChoices({
        name: "loading",
        value: "loading",
      })
  )
  .addStringOption((option) =>
    option
      .setName("開始時間")
      .setDescription("寫你要開始嘅時間")
      .setRequired(true)
  )
  .addIntegerOption((option) =>
    option
      .setName("活動次數")
      .setDescription("寫會打幾多次，唔寫即係打得幾多得幾多")
      .setRequired(false)
  );

const choppedRaids = env.getRaidsInLang().slice(0, 25);
newEvtCommand.options[0].choices = choppedRaids;

export default newEvtCommand.toJSON();
