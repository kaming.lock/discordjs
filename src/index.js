import env from "./env.js";
import {
  Client,
  Routes,
  GatewayIntentBits,
  Partials,
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
} from "discord.js";
import { REST } from "@discordjs/rest";
import NewEvtCommand from "./commands/newEvt.js";
import mongoose from "mongoose";
import eventSchema from "./../model/Event.js";

/**
 * Variables
 */

// Initialize discord client with access permissions
const client = new Client({
  intents: [
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
  ],
  partials: [Partials.Channel],
});

// Using a discord rest api
const rest = new REST({ version: "10" }).setToken(env.TOKEN);

// Define variables
const words = {
  unlimited: "無限",
  notReleased: "未推出",
};
const evtMsgAction = {
  noAction: "noAction",
  newEvt: "newEvt",
  leaveEvt: "leaveEvt",
};
const evtStatus = {
  created: "created",
  available: "available",
  notAvailable: "notAvailable",
  full: "full",
  started: "started",
  notFound: "notFound",
};

/**
 * Functions
 */

/**
 * Generate event message
 *
 * @param {String} action event message generating purpose
 * @param {String} user interaction user
 * @param {String} name event name
 * @param {Number} limit event limit amount of people
 * @param {String} time event starting time
 * @param {String} times times of event repeating
 * @param {[String]} participants event participants
 * @returns
 */
const genEvtMsg = (action, user, name, limit, time, times, participants) => {
  let newEvt = `${user} 起左新活動啦！\n`;
  let leaveEvt = `${user}唔打${name}啦！\n`;
  let evtName = `活動名稱：${name}`;
  let evtLimit = `人數限制：${limit}`;
  let evtTime = `開始時間：${time}`;
  let evtTimes = `活動次數：${times}`;
  let evtParticpants = `參與列表：\n${
    participants.length > 0 ? participants.join("\n") : "未有"
  }`;
  let basicMsg = [evtName, evtLimit, evtTime, evtTimes, evtParticpants].join(
    "\n"
  );
  if (action === evtMsgAction.newEvt) return [newEvt, basicMsg].join("\n");
  if (action === evtMsgAction.leaveEvt) return [leaveEvt, basicMsg].join("\n");
  return basicMsg;
};

/**
 * Get schedule time for reminding event starts
 * and the current time from event starting time
 *
 * @param {String} time event time in format of HH:mm
 * @returns
 */
const getScheduleTimeandCurrentTime = (time) => {
  let timeDigit = time.match(/^([01]?[0-9]|2[0-3]):([0-5][0-9])$/);
  let localTimeOffset = 8;
  let currentTime = new Date();
  currentTime.setHours(currentTime.getHours() + localTimeOffset);
  let scheduledTime = new Date();
  scheduledTime.setUTCHours(timeDigit[1], timeDigit[2], 0, 0);
  if (scheduledTime - currentTime <= 0) {
    scheduledTime.setDate(scheduledTime.getDate() + 1);
  }

  return [scheduledTime, currentTime];
};

/**
 * Create new event
 *
 * @param {Interaction} interaction discord user interaction
 */
const createNewEvt = (interaction) => {
  let raid = interaction.options.get("活動名稱").value;
  let time = interaction.options.get("開始時間").value;
  let times = interaction.options.get("活動次數")?.value;
  if (/^([01]?[0-9]|2[0-3]):([0-5][0-9])$/.test(time) !== true) {
    interaction.reply({
      content: `入番岩果時間好喎ching，要24小時格式啵！\n例如："15:26"`,
      ephemeral: true,
    });
    return;
  }
  if (!times) {
    times = words.unlimited;
  } else {
    times = `各${times}次`;
  }
  let raidData = env.getRaidsInLang(raid);
  let evtName = raidData.name;
  let evtLimit = raidData["limit" + env.pSrvCode_SUFFIX];
  interaction.reply({
    content: genEvtMsg(
      evtMsgAction.newEvt,
      interaction.user.toString(),
      evtName,
      evtLimit < 0
        ? words.unlimited
        : evtLimit === 0
        ? words.notReleased
        : evtLimit,
      time,
      times,
      []
    ),
    components: [
      new ActionRowBuilder().setComponents(
        new ButtonBuilder()
          .setCustomId("register")
          .setLabel("報名")
          .setStyle(ButtonStyle.Primary),
        new ButtonBuilder()
          .setCustomId("leave")
          .setLabel("退出")
          .setStyle(ButtonStyle.Danger)
      ),
    ],
  });
  setTimeout(async () => {
    const msg = await interaction.fetchReply();
    await new eventSchema({
      msgID: msg.id,
      name: evtName,
      limit: evtLimit,
      time: time,
      times: times,
      participants: [],
    }).save();
    console.log(`Event ID: ${msg.id} has been created.`);
  });
  let [scheduledTime, currentTime] = getScheduleTimeandCurrentTime(time);
  setTimeout(async () => {
    const msg = await interaction.fetchReply();
    let foundEvt = await eventSchema.findOne({ msgID: msg.id }).exec();
    interaction.followUp({
      content: `${foundEvt.participants.join("\n")}\n夠鐘打團啦！`,
    });
  }, scheduledTime - currentTime);
};

/**
 * Get event status
 *
 * @param {String} user discord interaction user
 * @param {Number} msgID message id
 * @param {String} participants participant
 */
const getEvtStatus = async (user, msgID, participants = "") => {
  let filter = { msgID: msgID };
  if (participants) filter.participants = participants;
  let foundEvt = await eventSchema.findOne(filter).exec();
  let statusInfo = {
    state: evtStatus.notFound,
    foundEvt: null,
    found: false,
  };

  if (!foundEvt) return statusInfo;
  statusInfo.state = evtStatus.created;
  statusInfo.foundEvt = foundEvt;
  statusInfo.found = true;
  if (foundEvt.limit === 0) {
    statusInfo.state = evtStatus.notAvailable;
    return statusInfo;
  }
  let [scheduledTime, currentTime] = getScheduleTimeandCurrentTime(
    foundEvt.time
  );
  console.log([scheduledTime, currentTime, scheduledTime - currentTime]);
  if (scheduledTime - currentTime <= 0) {
    statusInfo.state = evtStatus.started;
    return statusInfo;
  }
  foundEvt.participants = [...foundEvt.participants, user];
  foundEvt.participants = [...new Set(foundEvt.participants)];
  if (foundEvt.participants.length > foundEvt.limit) {
    statusInfo.state = evtStatus.full;
    return statusInfo;
  }
  statusInfo.state = evtStatus.available;
  statusInfo.foundEvt = foundEvt;
  return statusInfo;
};

/**
 * Application
 */

// Setup for application
client.on("ready", async () => {
  console.log(`${client.user.tag} has logged in.`);
  mongoose.connect(env.DATABASE_URI);
  console.log("Connecting to MongoDB...");
  mongoose.connection.on("open", () => {
    console.log("Connected to MongoDB");
    console.log("Everything is ready now!");
  });
});

// Engage with user interaction
client.on("interactionCreate", (interaction) => {
  /**
   * Commands
   */

  if (interaction.isCommand) {
    // newevent
    if (interaction.commandName === "newevent") {
      createNewEvt(interaction);
    }
  }

  /**
   * Buttons
   */

  if (interaction.isButton()) {
    // register / leave
    if (
      interaction.customId === "register" ||
      interaction.customId === "leave"
    ) {
      let msg = {
        content: `${interaction.user.toString()}正在耍智障…`,
        ephemeral: true,
      };
      let isAction = (action) => {
        return interaction.customId === action;
      };
      let leaveable = false;
      let args = [interaction.user, interaction.message.id];

      if (isAction("leave")) {
        args.push(interaction.user);
      }

      getEvtStatus(...args).then((evtStatusInfo) => {
        switch (evtStatusInfo.state) {
          case evtStatus.notFound:
            if (isAction("register")) {
              msg.content = `冇黎個活動架喎？你JM9？`;
            }
            break;
          case evtStatus.notAvailable:
            msg.content = `黎個活動未有得報名架喎？你JM9？`;
            break;
          case evtStatus.started:
            if (isAction("register")) {
              msg.content = `${interaction.user.toString()}宜家先黎...\n遲唔遲D呀？\n開始左啦！`;
            }
            leaveable = true;
            break;
          case evtStatus.full:
            if (isAction("register")) {
              msg.content = `滿哂啦！\n睇下有冇人願意退出啦，又或者㩒e條 [link](<https://bit.ly/3AzLyHp>) 一鍵強制加入！`;
            }
            leaveable = true;
            break;
          case evtStatus.available:
            if (isAction("register")) {
              msg.content = genEvtMsg(
                evtMsgAction.noAction,
                interaction.user.toString(),
                evtStatusInfo.foundEvt.name,
                evtStatusInfo.foundEvt.limit,
                evtStatusInfo.foundEvt.time,
                evtStatusInfo.foundEvt.times,
                evtStatusInfo.foundEvt.participants
              );
              msg.ephemeral = false;
              evtStatusInfo.foundEvt.save();
            }
            leaveable = true;
            break;
          case evtStatus.created:
          default:
            break;
        }

        if (isAction("leave") && leaveable === true) {
          console.log(evtStatusInfo.foundEvt.participants);
          evtStatusInfo.foundEvt.participants =
            evtStatusInfo.foundEvt.participants.filter((participant) => {
              return participant !== interaction.user.toString();
            });
          msg.content = genEvtMsg(
            evtMsgAction.leaveEvt,
            interaction.user.toString(),
            evtStatusInfo.foundEvt.name,
            evtStatusInfo.foundEvt.limit,
            evtStatusInfo.foundEvt.time,
            evtStatusInfo.foundEvt.times,
            evtStatusInfo.foundEvt.participants
          );
          msg.ephemeral = false;
          evtStatusInfo.foundEvt.save();
        }
        if (!interaction.replied) interaction.reply(msg);
        else interaction.followUp(msg);
      });
    }
  }
});

/**
 * Main Program
 */
async function main() {
  /**
   * Slash Commands
   */

  const commands = [NewEvtCommand];
  try {
    console.log("Started refreshing application (/) commands.");
    await rest.put(
      Routes.applicationGuildCommands(env.CLIENT_ID, env.GUILD_ID),
      {
        body: commands,
      }
    );
    client.login(env.TOKEN);
  } catch (err) {
    console.log(err);
  }
}

// runs the main program
main();
